##Übung 9

#Aufgabe 1

Machen Sie sich mit Markdown vertraut. Markdown ist rein textbasiert, das heißt, dass Formatierungen nicht durch Befehle / Tags wie bei HTML beschrieben werden, sondern nur durch Text selbst. Lesen Sie https://drdanielappel.de/tipps-tools/markdown-eine-einfach-zu-erlernende-auszeichnungssprache/ .

1. Erstellen Sie eine Markdown-Datei (Endung .md), kopieren Sie den Text dieser Aufgabe hinein und formatieren Sie ihn so, dass er möglichst wie in dieser HTML-Seite aussieht.

| Syntax    | Description |
| --------- | ----------- |
| Header    | Title       |
| Paragraph | Text        |

|  Gruppe A   | Gruppe B | Gruppe C  |
| :---------: | :------: | :-------: |
| Deutschland | Albanien | Dänemark  |
|   Schweiz   | Italien  |  England  |
| Schottland  | Kroatien |  Serbien  |
|   Schweiz   | Spanien  | Slowenien |
